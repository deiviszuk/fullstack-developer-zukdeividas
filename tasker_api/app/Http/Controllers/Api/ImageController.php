<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Storage;

class ImageController extends Controller
{
    public function getImages()
    {
        return response()->json(Auth::user()->images->toArray());
    }

    public function uploadImages(Request $request)
    {
        $file = $request->file('file');
        $ext = $file->extension();
        $name = str_random(20) . '.' . $ext;
        list($width, $height) = getimagesize($file);
        $path = Storage::disk('public')->putFileAs(
            'uploads', $file, $name
        );
        if ($path) {
            $create = Auth::user()->images()->create([
                'uri' => $path,
                'public' => false,
                'height' => $height,
                'width' => $width,
            ]);

            if ($create) {
                return response()->json([
                    'uploaded' => true,
                ]);
            }
        }
    }
}
