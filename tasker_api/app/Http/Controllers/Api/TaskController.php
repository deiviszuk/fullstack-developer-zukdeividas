<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Task;
use Auth;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::where('user_id', '=', Auth::user()->id)->orderBy('importance', 'desc')->get();
        return response()->json($tasks);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task;
        $task->user_id = Auth::user()->id;
        $task->title = $request->input('title');
        $task->description = $request->input('description');
        $task->date = $request->input('date');
        $task->time_from = $request->input('time_from');
        $task->time_to = $request->input('time_to');
        $task->location_long = $request->input('location_long');
        $task->location_lat = $request->input('location_lat');
        $task->notify_before = $request->input('notify_before');
        $task->notification_label = $request->input('notification_label');
        $task->importance = $request->input('importance');
        $task->finished = $request->input('finished');
        $task->location = $request->input('location');
        $task->save();
        return response()->json($task);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        if ($task->user_id !== Auth::user()->id) {
            return response()->json(['success' => false, 'message' => 'Access denied'], 403);
        }

        $task = Task::where('id', '=', $task->id)->first();
        $task->title = $request->input('title');
        $task->description = $request->input('description');
        $task->date = $request->input('date');
        $task->time_from = $request->input('time_from');
        $task->time_to = $request->input('time_to');
        $task->location_long = $request->input('location_long');
        $task->location_lat = $request->input('location_lat');
        $task->notify_before = $request->input('notify_before');
        $task->notification_label = $request->input('notification_label');
        $task->importance = $request->input('importance');
        $task->finished = $request->input('finished');
        $task->location = $request->input('location');
        $task->save();
        return response()->json($task);
    }

    public function finish(Request $request, Task $task)
    {
        if ($task->user_id !== Auth::user()->id) {
            return response()->json(['success' => false, 'message' => 'Access denied'], 403);
        }

        $task = Task::where('id', '=', $task->id)->first();
        $task->finished = $request->input('true');
        $task->save();

        return response()->json($task, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        if ($task->user_id !== Auth::user()->id) {
            return response()->json(['success' => false, 'message' => 'Access denied'], 403);
        }

        $task = Task::where('id', '=', $task->id)->first();
        $task->delete();

        return response()->json(['success' => true], 200);
    }
}
