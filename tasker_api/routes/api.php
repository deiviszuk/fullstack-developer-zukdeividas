<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::post('login', [ 'as' => 'login', 'uses' => 'API\UserController@login']);
Route::post('register', 'API\UserController@register');


Route::group(['middleware' => 'auth:api'], function () {

    Route::put('finish/{task}', 'Api\TaskController@finish');

    Route::post('logout','Api\UserController@logout');

    Route::resource('tasks', 'Api\TaskController', [
        'only' => [
            'index', 'store', 'update', 'destroy'
        ]
    ]);

    Route::get('images', 'Api\ImageController@getImages');

    Route::post('images', 'Api\ImageController@uploadImages');

});
