import React, { Component } from 'react';
import TaskList from './../components/TaskList';
import axios from 'axios';
import Gallery from 'react-photo-gallery';

export default class Home extends Component {
    state = {
        tasks: true,
        selectedFile: null,
        loaded: 0,
        images: []
    };

    componentDidMount() {
        axios
            .get('/api/images', {
                headers: {
                    authorization: 'Bearer ' + localStorage.getItem('token'),
                    accept: 'application/json',
                    'content-type': 'application/x-www-form-urlencoded'
                }
            })
            .then(response => {
                const images = response.data;
                console.log(response.data);
                this.setState({
                    images: images
                });
            });
    }

    handleselectedFile = event => {
        this.setState({
            selectedFile: event.target.files[0],
            loaded: 0
        });
    };

    handleUpload = () => {
        const data = new FormData();
        data.append(
            'file',
            this.state.selectedFile,
            this.state.selectedFile.name
        );

        axios
            .post(
                '/api/images',
                data,
                {
                    headers: {
                        authorization:
                            'Bearer ' + localStorage.getItem('token'),
                        accept: 'application/json',
                        'content-type': 'application/x-www-form-urlencoded'
                    }
                },
                {
                    onUploadProgress: ProgressEvent => {
                        this.setState({
                            loaded:
                                (ProgressEvent.loaded / ProgressEvent.total) *
                                100
                        });
                    }
                }
            )
            .then(res => {
                console.log(res.statusText);
            });
    };

    images() {
        if (this.images !== null) {
            return <Gallery photos={this.images} />;
        }
        return '';
    }

    render() {
        let photos = this.state.images.map(image => {
            return {
                src : '/storage/' + image.uri,
                width : image.width,
                height : image.height,
                id :  image.id
            }
        });
        return (
            <div className='main-screen'>
                <div className='file-input'>
                    <input type='file' onChange={this.handleselectedFile} />
                    <button onClick={this.handleUpload}>Upload</button>
                    <div> {Math.round(this.state.loaded, 2)} %</div>
                    {this.state.images.length ?
                        <Gallery
                            photos={photos}
                        />
                        :
                        <div className="no-images">
                            <h5 className="text-center">
                                You currently have no images in your photos gallery
                            </h5>
                        </div>
                    }
                </div>

                <TaskList />
            </div>
        );
    }
}
