import React, { Component } from "react";
import { Container, Row, Col, Input, Button } from "mdbreact";
import { Link, Redirect } from "react-router-dom";

export default class Login extends Component {
    state = {
        email: "",
        password: "",
        redirect: false
    };

    handleInput = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    handleSubmit = () => {
        axios
            .post("/api/login/", {
                email: this.state.email,
                password: this.state.password
            })
            .then(response => {
                if (response.data.success) {
                    localStorage.setItem("token", response.data.success.token);
                    this.setState({ redirect: true });
                }
                if (!response.data.success) {
                    console.log(response);
                }
            });
    };

    render() {
        if (this.state.redirect) {
            return <Redirect to={"/"} />;
        }
        return (
            <Container className="vertical-center">
                <Row className="justify-content-md-center login-form">
                    <Col md="6">
                        <form>
                            <p className="h5 text-center mb-4">Login</p>
                            <div className="grey-text">
                                <Input
                                    onChange={this.handleInput}
                                    label="Type your email"
                                    icon="envelope"
                                    group
                                    type="email"
                                    validate
                                    error="wrong"
                                    success="right"
                                    name="email"
                                />
                                <Input
                                    label="Type your password"
                                    icon="lock"
                                    group
                                    type="password"
                                    validate
                                    onChange={this.handleInput}
                                    name="password"
                                />
                            </div>
                            <div className="text-center">
                                <Button onClick={this.handleSubmit}>
                                    Login
                                </Button>
                            </div>
                            <p className="h7 text-center">
                                Not registered?&nbsp;
                                <Link to={"/register"}>Register!</Link>
                            </p>
                        </form>
                    </Col>
                </Row>
            </Container>
        );
    }
}
