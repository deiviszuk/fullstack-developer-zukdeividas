import React, { Component } from "react";
import { Container, Row, Col, Input, Button } from "mdbreact";
import { Link, Redirect } from "react-router-dom";

export default class Register extends Component {
    state = {
        name: "",
        email: "",
        password: "",
        confirm_password: "",
        redirect: false
    };

    handleInput = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    handleSubmit = () => {
        axios
            .post("/api/register/", {
                name: this.state.name,
                email: this.state.email,
                password: this.state.password,
                c_password: this.state.confirm_password
            })
            .then(response => {
                if (response.data.success) {
                    this.setState({ redirect: true });
                }
                if (!response.data.success) {
                    console.log(response);
                }
            });
    };

    render() {
        if (this.state.redirect) {
            return <Redirect to={"/login"} />;
        }
        return (
            <Container className="vertical-center">
                <Row className="justify-content-md-center login-form">
                    <Col md="6">
                        <form>
                            <p className="h5 text-center mb-4">Register</p>
                            <div className="grey-text">
                                <div className="grey-text">
                                    <Input
                                        required
                                        label="Your name"
                                        icon="user"
                                        group
                                        type="text"
                                        validate
                                        error="wrong"
                                        success="right"
                                        name="name"
                                        onChange={this.handleInput}
                                    />
                                    <Input
                                        required
                                        label="Your email"
                                        icon="envelope"
                                        group
                                        type="email"
                                        validate
                                        error="wrong"
                                        success="right"
                                        name="email"
                                        onChange={this.handleInput}
                                    />
                                    <Input
                                        required
                                        label="Your password"
                                        icon="lock"
                                        group
                                        type="password"
                                        validate
                                        error="wrong"
                                        success="right"
                                        name="password"
                                        onChange={this.handleInput}
                                    />
                                    <Input
                                        required
                                        label="Confirm your password"
                                        icon="exclamation-triangle"
                                        group
                                        type="password"
                                        validate
                                        error="wrong"
                                        success="right"
                                        name="confirm_password"
                                        onChange={this.handleInput}
                                    />
                                </div>
                            </div>
                            <div className="text-center">
                                <Button onClick={this.handleSubmit}>
                                    Register
                                </Button>
                            </div>
                            <p className="h7 text-center">
                                Have account?&nbsp;
                                <Link to={"/login"}>Login!</Link>
                            </p>
                        </form>
                    </Col>
                </Row>
            </Container>
        );
    }
}
