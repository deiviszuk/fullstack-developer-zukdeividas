import React, { Component } from "react";
import { Container, Row, Col, Input, Button } from "mdbreact";
import { Redirect } from "react-router-dom";
import DateTime from "react-datetime";
import moment from "moment";
import querystring from "querystring";
import "react-datetime/css/react-datetime.css";

export default class AddTask extends Component {
    state = {
        title: "",
        description: "",
        importance: null,
        time_from: null,
        time_to: null,
        location: null,
        redirect: false
    };

    handleInput = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    handleDatetime = (name, e) => {
        this.setState({ [name]: moment(e).format("YYYY-MM-DD HH:mm") });
    };

    handleSubmit = () => {
        axios
            .post(
                "/api/tasks",
                querystring.stringify({
                    title: this.state.title,
                    description: this.state.description,
                    importance: this.state.importance,
                    time_from: this.state.time_from,
                    time_to: this.state.time_to,
                    location: this.state.location
                }),
                {
                    headers: {
                        authorization:
                            "Bearer " + localStorage.getItem("token"),
                        accept: "application/json",
                        "content-type": "application/x-www-form-urlencoded"
                    }
                }
            )
            .then(response => {
                if (response.status === 200) {
                    this.setState({ redirect: true });
                }
                if (!response.data.success) {
                    console.log(response);
                }
            });
    };

    render() {
        if (this.state.redirect) {
            return <Redirect to={"/"} />;
        }
        return (
            <Container className="vertical-center">
                <Row className="justify-content-md-center login-form">
                    <Col md="6">
                        <form>
                            <h1 className="h5 text-center mb-4">Add Task</h1>
                            <div className="grey-text">
                                <br />
                                <Input
                                    onChange={this.handleInput}
                                    label="Enter title"
                                    icon="tag"
                                    group
                                    type="text"
                                    name="title"
                                    required
                                />
                                <br />
                                <Input
                                    label="Enter description"
                                    icon="pencil"
                                    type="textarea"
                                    onChange={this.handleInput}
                                    name="description"
                                    required
                                />
                                <Input
                                    label="Importance from 1-5"
                                    icon="exclamation"
                                    type="number"
                                    onChange={this.handleInput}
                                    name="importance"
                                    min="1"
                                    max="5"
                                    required
                                />

                                <div className="datetime-picker row">
                                    <i className="far fa-clock fa-2x" />
                                    &nbsp;&nbsp;From:&nbsp;&nbsp;
                                    <DateTime
                                        dateFormat={"YY-MM-DD"}
                                        locale={"lt"}
                                        timeFormat={"HH:mm"}
                                        selected={this.state.startDate}
                                        onChange={e =>
                                            this.handleDatetime("time_from", e)
                                        }
                                        name="time_from"
                                    />
                                    &nbsp;&nbsp;&nbsp;&nbsp;To:&nbsp;&nbsp;
                                    <DateTime
                                        dateFormat={"YY-MM-DD"}
                                        locale={"lt"}
                                        timeFormat={"HH:mm"}
                                        selected={this.state.startDate}
                                        onChange={e =>
                                            this.handleDatetime("time_to", e)
                                        }
                                        name="time_to"
                                    />
                                </div>
                                <Input
                                    onChange={this.handleInput}
                                    label="Enter location"
                                    icon="map"
                                    group
                                    type="text"
                                    name="location"
                                />
                            </div>

                            <div className="text-center">
                                <Button onClick={() => this.handleSubmit()}>
                                    Create
                                </Button>
                            </div>
                        </form>
                    </Col>
                </Row>
            </Container>
        );
    }
}
