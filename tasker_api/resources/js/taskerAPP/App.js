import React, { Fragment } from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import Login from "./pages/Login";
import Register from "./pages/Register";
import Home from "./pages/Home";
import AddTask from "./pages/AddTask";

const App = () => (
    <Fragment>
        <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/" component={Home} />
            <Route exact path="/add" component={AddTask} />
        </Switch>
    </Fragment>
);

export default App;
