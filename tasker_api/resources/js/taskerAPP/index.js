import React from "react";
import { render } from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";
import App from "./App";
import Navigation from "./components/Navigation";

import "font-awesome/css/font-awesome.min.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";

const Side = (props) => {
    if (props.location.pathname == "/login" || props.location.pathname == "/register") {
    return null;
    }
    return <Navigation location={props.location} history={props.history} />;
};

render(
    <Router>
        <Route
            render={({ location, history }) => (
                <React.Fragment>
                    <Side location={location} history={history}/>
                    <main>
                        <App />
                    </main>
                </React.Fragment>
            )}
        />
    </Router>,
    document.getElementById("app")
);
