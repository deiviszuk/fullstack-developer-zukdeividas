import React, { Component } from "react";
import SideNav, {
    Toggle,
    Nav,
    NavItem,
    NavIcon,
    NavText
} from "@trendmicro/react-sidenav";
import "@trendmicro/react-sidenav/dist/react-sidenav.css";

export default class Navigation extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    handleLogout = () => {
        console.log("logout");

        // Meta ERR reikia sutvarkyti
        // axios
        //     .post("api/logout", {
        //         headers: {
        //             authorization: "Bearer " + localStorage.getItem("token"),
        //             accept: "application/json",
        //             "content-type": "application/x-www-form-urlencoded"
        //         }
        //     })
        //     .then(response => {
        //         if (response.status === 401) {
        //             console.log(response);
        // localStorage.removeItem("token");
        // this.props.history.push("/login");
        //         }
        //         if (response.status !== 401) {
        //             console.log(response);
        //         }
        //     })
        //     .catch(error => console.log(error));
    };

    render() {
        return (
            <SideNav
                id="navigation"
                onSelect={selected => {
                    const to = "/" + selected;
                    if (selected == "logout") {
                        localStorage.removeItem("token");
                        this.props.history.push("/login");
                        return;
                    }
                    if (location.pathname !== to) {
                        this.props.history.push(to);
                    }
                }}
            >
                <SideNav.Toggle />
                <SideNav.Nav defaultSelected="">
                    <NavItem eventKey="">
                        <NavIcon>
                            <i
                                className="fa fa-fw  fa-calendar-check-o"
                                style={{ fontSize: "1.75em" }}
                            />
                        </NavIcon>
                        <NavText>Home</NavText>
                    </NavItem>
                    <NavItem eventKey="add">
                        <NavIcon>
                            <i
                                className="fa fa-fw fa-calendar-plus-o"
                                style={{ fontSize: "1.75em" }}
                            />
                        </NavIcon>
                        <NavText>Add New</NavText>
                    </NavItem>
                    <NavItem eventKey="logout">
                        <NavIcon>
                            <i
                                className="fa fa-fw fa-sign-out"
                                style={{ fontSize: "1.75em" }}
                            />
                        </NavIcon>
                        <NavText>Logout</NavText>
                    </NavItem>
                </SideNav.Nav>
            </SideNav>
        );
    }
}
