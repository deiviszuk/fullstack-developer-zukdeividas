import React, { Component, Fragment } from "react";
import Timestamp from "react-timestamp";
import {
    Button,
    Modal,
    ModalBody,
    ModalHeader,
    ModalFooter,
    Row
} from "mdbreact";
import querystring from "querystring";

import TaskDetails from "./TaskDetails";
import TaskEdit from "./TaskEdit";

export default class TaskItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            edit: false
        };
    }

    toggleModal = () => {
        this.setState({
            modal: !this.state.modal
        });
    };

    toggleEdit = () => {
        this.setState({
            edit: !this.state.edit
        });
    };

    handleFormSubmit = params => {
        axios
            .put(
                `/api/tasks/${this.props.data.id}`,
                querystring.stringify(params),
                {
                    headers: {
                        authorization:
                            "Bearer " + localStorage.getItem("token"),
                        accept: "application/json",
                        "content-type": "application/x-www-form-urlencoded"
                    }
                }
            )
            .then(response => {
                if (response.status === 200) {
                    this.setState({
                        modal: false,
                        edit: false
                    });
                    this.props.fetchTasks();
                }
                if (response.status !== 200) {
                    console.log(response);
                }
            });
    };

    handleDelete = () => {
        axios
            .delete(`/api/tasks/${this.props.data.id}`, {
                headers: {
                    authorization: "Bearer " + localStorage.getItem("token"),
                    accept: "application/json",
                    "content-type": "application/x-www-form-urlencoded"
                }
            })
            .then(response => {
                if (response.status === 200) {
                    this.setState({
                        modal: false,
                        edit: false
                    });
                    this.props.fetchTasks();
                }
                if (response.status !== 200) {
                    console.log(response);
                }
            });
    };

    finish = () => {
        const finished = this.props.data.finished;
        console.log(finished);

        if (finished) {
            return (
                <div className="header-finish" style={{ float: "right" }}>
                    <i className="fas fa-check finished-button" />
                </div>
            );
        }

        return (
            <div onClick={this.handleFinish} className="header-finish" style={{ float: "right" }}>
                <i className="fas fa-flag-checkered tofinish-button" />
            </div>
        );
    };

    //nepabaigta 
    handleFinish = () => {
        const task = this.props.data.id;
        alert(id);

    }

    render() {
        return (
            <Fragment>
                <Modal isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalHeader toggle={this.toggle}>
                        {this.props.data.title}
                    </ModalHeader>
                    <ModalBody>
                        {this.state.edit ? (
                            <TaskEdit
                                task={this.props.data}
                                formSubmit={this.handleFormSubmit}
                            />
                        ) : (
                            <TaskDetails task={this.props.data} />
                        )}
                    </ModalBody>
                    <ModalFooter>
                        {this.state.edit ? null : (
                            <Fragment>
                                <Button
                                    color="danger"
                                    onClick={this.handleDelete}
                                >
                                    Delete
                                </Button>{" "}
                                <Button
                                    onClick={this.toggleEdit}
                                    color="warning"
                                >
                                    Edit
                                </Button>
                            </Fragment>
                        )}
                        <Button
                            color="secondary"
                            onClick={() => {
                                this.setState({
                                    modal: false,
                                    edit: false
                                });
                            }}
                        >
                            Close
                        </Button>{" "}
                    </ModalFooter>
                </Modal>

                <div onClick={this.toggleModal} className="card task-card">
                    <div
                        className={`task-importance-${
                            this.props.data.importance
                        }`}
                    />
                    <div className="task-body">
                        <div className="header-container">
                            <div
                                className="header-title"
                                style={{ float: "left" }}
                            >
                                <h2>
                                    <b>{this.props.data.title}</b>
                                </h2>
                            </div>
                            {this.finish()}
                        </div>
                        <div style={{ clear: "both" }} />

                        {this.props.data.time_from ||
                        this.props.data.time_to ? (
                            <div>
                                <h6>
                                    <i className="far fa-clock" />{" "}
                                    <Timestamp
                                        time={this.props.data.time_from}
                                        format="time"
                                    />{" "}
                                    -{" "}
                                    <Timestamp
                                        time={this.props.data.time_to}
                                        format="time"
                                    />
                                </h6>
                            </div>
                        ) : null}
                        {this.props.data.location ? (
                            <h6>
                                <i className="fas fa-map-marker-alt" />{" "}
                                {this.props.data.location}
                            </h6>
                        ) : null}
                    </div>
                </div>
            </Fragment>
        );
    }
}
