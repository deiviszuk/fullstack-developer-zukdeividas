import React, { Component } from "react";
import { Container, Input, Button } from "mdbreact";
import DateTime from "react-datetime";
import moment from "moment";
import "react-datetime/css/react-datetime.css";

export default class TaskEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.task.title,
            description: this.props.task.description,
            importance: this.props.task.importance,
            time_from: this.props.task.time_from,
            time_to: this.props.task.time_to,
            location: this.props.task.location
        };
    }

    handleInput = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    handleDatetime = (name, e) => {
        this.setState({ [name]: moment(e).format("YYYY-MM-DD HH:mm") });
    };

    handleSubmit = () => {
        let data = {
            title: this.state.title,
            description: this.state.description,
            importance: this.state.importance,
            time_from: this.state.time_from,
            time_to: this.state.time_to,
            location: this.state.location
        };
        this.props.formSubmit(data);
    };

    render() {
        return (
            <Container>
                <form>
                    <div className="grey-text">
                        <br />
                        <Input
                            value={this.state.title}
                            onChange={this.handleInput}
                            label="Enter title"
                            icon="tag"
                            type="text"
                            name="title"
                        />
                        <Input
                            value={this.state.description}
                            label="Enter description"
                            icon="pencil"
                            type="textarea"
                            onChange={this.handleInput}
                            name="description"
                        />
                        <Input
                            value={"" + this.state.importance}
                            label="Importance from 1-5"
                            icon="exclamation"
                            type="number"
                            onChange={this.handleInput}
                            name="importance"
                            min="1"
                            max="5"
                        />
                        <div>
                            <i className="far fa-clock fa-2x" />
                            &nbsp;&nbsp;From:&nbsp;&nbsp;
                            <DateTime
                                dateFormat={"YY-MM-DD"}
                                locale={"lt"}
                                timeFormat={"HH:mm"}
                                selected={this.state.startDate}
                                onChange={e =>
                                    this.handleDatetime("time_from", e)
                                }
                                name="time_from"
                            />
                            &nbsp;&nbsp;&nbsp;&nbsp;To:&nbsp;&nbsp;
                            <DateTime
                                dateFormat={"YY-MM-DD"}
                                locale={"lt"}
                                timeFormat={"HH:mm"}
                                selected={this.state.startDate}
                                onChange={e =>
                                    this.handleDatetime("time_to", e)
                                }
                                name="time_to"
                            />
                        </div>
                        <Input
                            value={this.state.location}
                            onChange={this.handleInput}
                            label="Enter location"
                            icon="map"
                            group
                            type="text"
                            name="location"
                        />
                    </div>
                    <div className="text-center">
                        <Button onClick={this.handleSubmit}>Update</Button>
                    </div>
                </form>
            </Container>
        );
    }
}
