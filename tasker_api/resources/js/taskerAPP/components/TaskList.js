import React, { Component, Fragment } from "react";
import { HashLoader } from "react-spinners";
import { css } from "react-emotion";
import { Redirect } from "react-router-dom";

import TaskItem from "./TaskItem";

const override = css`
    min-height: 100%;
    min-height: 100vh;
    display: flex;
    margin: auto;
    align-items: center;
`;

export default class TaskList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: null,
            redirect: false
        };
    }

    fetchTasks = () => {
        console.log("Fetch");
        axios
            .get("/api/tasks", {
                headers: {
                    authorization: "Bearer " + localStorage.getItem("token"),
                    accept: "application/json",
                    "content-type": "application/x-www-form-urlencoded"
                }
            })
            .then(response => {
                this.setState({
                    tasks: response.data,
                    loading: false
                });
            })
            .catch(error => {
                this.setState({
                    redirect: true
                });
            });
    };

    componentDidMount() {
        this.setState({ loading: true });
        this.fetchTasks();
    }

    renderTasks = () => {
        return this.state.tasks.map(task => {
            return (
                <TaskItem
                    itemClick={this.onItemClick}
                    key={task.id}
                    data={task}
                    fetchTasks={this.fetchTasks}
                />
            );
        });
    };

    render() {
        if (this.state.redirect) {
            return <Redirect to="/login" />;
        }
        return (
            <Fragment>
                {!this.state.tasks ? (
                    <HashLoader
                        className={override}
                        sizeUnit={"px"}
                        size={100}
                        color={"#db3d44"}
                        loading={true}
                    />
                ) : (
                    <div className="tasks-container">{this.renderTasks()}</div>
                )}
            </Fragment>
        );
    }
}
