import React from "react";
import { Container } from "mdbreact";
import moment from "moment";

export default class TaskDetails extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Container>
                <i className="fas fa-pencil-alt">
                    &nbsp;Description:&nbsp;&nbsp;
                </i>
                <p>{this.props.task.description}</p>
                <i className="fas fa-exclamation-triangle">
                    &nbsp;Importance:&nbsp;&nbsp;
                </i>
                <p>{this.props.task.importance}</p>
                <i className="far fa-clock">&nbsp;Time:&nbsp;&nbsp;</i>
                <div className="row">
                    <p>{moment(this.props.task.time_from).format("HH:mm")}</p>
                    &nbsp;&nbsp;-&nbsp;&nbsp;
                    <p>{moment(this.props.task.time_to).format("HH:mm")}</p>
                </div>
                <i className="fas fa-map-marked-alt">
                    &nbsp;Location:&nbsp;&nbsp;
                </i>
                <p>{this.props.task.location}</p>
            </Container>
        );
    }
}
