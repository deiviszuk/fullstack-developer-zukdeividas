# Fullstack Developer Chalenge

## Installation
1. Clone repository
2. change .env 
3. composer install
4. npm install
5. php artisan migrate
6. php artisan passport:install
7. php artisan key:generate
8. php artisan serve
9. npm run watch
10. For images <php artisan storage:link>

## To do
1. Finish task
2. Logout from backend
3. Find task's time sol
4. MAPS integration
5. And many stuff more :D